namespace weatherreport
{
    class WeatherData
    {
        public string Day {get; set;}
        public double Temp {get; set;}
        public int Rain {get; set;}
    }
}
