using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace weatherreport
{
    class WeatherReport
    {
        public void PrintWeatherReport()
        {
            var data = DownloadWeatherData();
            foreach(var day in data)
            {
                Console.WriteLine($"{day.Day}: {day.Temp}");
            }
        }

        public IEnumerable<WeatherData> DownloadWeatherData()
        {
            string json;
            try
            {
                json = WebRequest.GETRequest(
                    "http://my-weatherservice.com/api/forecast/fivedays");
            }
            catch
            {
                return new List<WeatherData>();
            }

            return JsonConvert.DeserializeObject<RestData>(json).FiveDays;
        }

        class RestData{
            public List<WeatherData> FiveDays {get;set;}
        }
    }
}
