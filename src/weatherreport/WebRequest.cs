namespace weatherreport
{
    static class WebRequest
    {
        const string RestData = @"{
            'FiveDays' : [
                {
                    'Day': 'saturday',
                    'Temp': 20.0,
                    'Rain' : 0
                },
                {
                    'Day': 'sunday',
                    'Temp': 21.0,
                    'Rain' : 0
                },
                {
                    'Day': 'monday',
                    'Temp': 17.0,
                    'Rain' : 40
                },
                {
                    'Day': 'tuesday',
                    'Temp': 15.0,
                    'Rain' : 30
                },
                {
                    'Day': 'wednesday',
                    'Temp': 8.0,
                    'Rain' : 0
                },
            ]
        }";

        public static string GETRequest(string url)
        {
            return RestData;
        }
    }
}
